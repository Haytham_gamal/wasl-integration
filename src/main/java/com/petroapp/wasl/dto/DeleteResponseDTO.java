package com.petroapp.wasl.dto;


import lombok.Data;

@Data
public class DeleteResponseDTO {

	private String resultCode ;
	private boolean success ;
	private Result result ;
	
	
	@Data
	private static class Result {
		private Boolean isValid ;
	}
}
