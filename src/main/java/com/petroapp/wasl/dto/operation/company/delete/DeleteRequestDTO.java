package com.petroapp.wasl.dto.operation.company.delete;

import javax.validation.constraints.NotBlank;

import lombok.Data;

@Data
public class DeleteRequestDTO {

	@NotBlank(message = "referenceKey is mandatory")
	private String referenceKey ;
}
