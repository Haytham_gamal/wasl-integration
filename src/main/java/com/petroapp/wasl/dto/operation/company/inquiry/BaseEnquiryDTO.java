package com.petroapp.wasl.dto.operation.company.inquiry;

import javax.validation.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeInfo.As;
import com.fasterxml.jackson.annotation.JsonTypeInfo.Id;

import lombok.Data;

@Data
@JsonTypeInfo(use = Id.NAME , include = As.PROPERTY , property = "companyType")
@JsonSubTypes({
	@JsonSubTypes.Type(value = CompanyInquiryRequestDTO.class , name = "COMPANY"),
})
public abstract class BaseEnquiryDTO {

	@NotBlank(message = "identityNumber is mandatory")
	private String identityNumber ;
}
