package com.petroapp.wasl.dto.operation.company.inquiry;

import lombok.Data;

@Data
public class InquiryResponseDTO {

	private String referenceNumber ;
	private String name ; 
	private String registrationDate ;
	private Boolean isValid ;
}
