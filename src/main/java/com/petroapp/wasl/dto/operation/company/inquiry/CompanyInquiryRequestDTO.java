package com.petroapp.wasl.dto.operation.company.inquiry;

import javax.validation.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonTypeName;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@JsonTypeName("COMPANY")
public class CompanyInquiryRequestDTO extends BaseEnquiryDTO {

	@NotBlank(message = "commercialRecordNumber is mandatory")
	private String commercialRecordNumber ;
}
