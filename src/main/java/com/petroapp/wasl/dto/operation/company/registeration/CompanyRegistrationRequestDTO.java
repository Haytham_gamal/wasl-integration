package com.petroapp.wasl.dto.operation.company.registeration;

import javax.validation.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonTypeName;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@JsonTypeName("COMPANY")
public class CompanyRegistrationRequestDTO extends BaseRegisterDTO{

	@NotBlank(message = "commercialRecordNumber is mandatory")
	private String commercialRecordNumber ;
	
	@NotBlank(message = "commercialRecordIssueDateHijri is mandatory")
	private String commercialRecordIssueDateHijri ;
	
	@NotBlank(message = "managerName is mandatory")
	private String managerName ;
	
	@NotBlank(message = "managerPhoneNumber is mandatory")
	private String managerPhoneNumber; 
	
	@NotBlank(message = "managerMobileNumber is mandatory")
	private String managerMobileNumber ;
}
