package com.petroapp.wasl.dto.operation.company.update;

import lombok.Data;

@Data
public class UpdateContactInfoResponseDTO {

	private String resultCode;
	private Boolean success ;
}
