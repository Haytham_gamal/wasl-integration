package com.petroapp.wasl.dto.operation.company.registeration;


import com.fasterxml.jackson.annotation.JsonTypeName;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@JsonTypeName("INDIVIDUAL")
public class IndividualRegisterationRequestDTO extends BaseRegisterDTO{

	private String dateOfBirthHijri ;
	private String dateOfBirthGregorian ;
}
