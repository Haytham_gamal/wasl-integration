package com.petroapp.wasl.dto.operation.company.registeration;

import javax.validation.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeInfo.As;
import com.fasterxml.jackson.annotation.JsonTypeInfo.Id;

import lombok.Data;

@Data
@JsonTypeInfo(use = Id.NAME , include = As.PROPERTY , property = "companyType")
@JsonSubTypes({
	@JsonSubTypes.Type(value = CompanyRegistrationRequestDTO.class , name = "COMPANY"),
	@JsonSubTypes.Type(value = IndividualRegisterationRequestDTO.class , name = "INDIVIDUAL")
})
public abstract class BaseRegisterDTO {

	@NotBlank(message = "identityNumber is mandatory")
	private String identityNumber ;
	
	@NotBlank(message = "phoneNumber is mandatory")
	private String phoneNumber ;
	
	private String extensionNumber ;
	
	@NotBlank(message = "emailAddress is mandatory")
	private String emailAddress ;
	
}
