package com.petroapp.wasl.dto.operation.company.update;

import javax.validation.constraints.NotBlank;

import lombok.Data;

@Data
public class UpdateContactInfoRequestDTO {

	@NotBlank(message = "identityNumber Can't be blank")
	private String identityNumber ;
	
	@NotBlank(message = "commercialRecordNumber Can't be blank")
	private String commercialRecordNumber ;
	
	@NotBlank(message = "managerName Can't be blank")
	private String managerName ;
	
	@NotBlank(message = "managerPhoneNumber Can't be blank")
	private String managerPhoneNumber ;
	
	@NotBlank(message = "managerMobileNumber Can't be blank")
	private String managerMobileNumber ;
}
