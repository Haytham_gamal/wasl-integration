package com.petroapp.wasl.dto.vehicle.live.location;

import lombok.Data;

@Data
public class LiveLocationResponseDTO {

	private String resultCode ;
	private boolean success ;
	private Result result ;
	
	
	@Data
	private static class Result {
		private String[] failedVehicles ;
	}
}
