package com.petroapp.wasl.dto.vehicle.inquiry;

import lombok.Data;

@Data
public class InquiryVehicleResponseDTO {

	private String referenceKey ;
	private Plate plate ;
	private VehicleLocationInformation vehicleLocationInformation ;
	private String registrationTime ;
	private OperationCompany[] operatingCompanies ;
	
	@Data
	private class Plate {
		private String number ;
		private String rightLetter ;
		private String middleLetter ;
		private String leftLetter ;
	}
	
	@Data
	private class VehicleLocationInformation {
		private Double latitude ;
		private Double longitude ;
		private String vehicleStatus ;
		private String actualTime ;
		private String receivedTime ;
	}
	
	@Data
	private class OperationCompany {
		private String referenceKey ;
		private String name ;
		private Boolean isVehicleValid ;
		private String vehicleRejectionReason ;
	}
}
