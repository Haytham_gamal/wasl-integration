package com.petroapp.wasl.dto.vehicle.register;

import lombok.Data;

@Data
public class IMEIUpdateRequestDTO {

	private String sequenceNumber ;
	private String imeiNumber ;
	
}
