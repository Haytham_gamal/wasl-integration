package com.petroapp.wasl.dto.vehicle.register;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class RegisterVehicleResponseDTO  {

	private Boolean success ;
	private String resultCode ;
	private Result result ;
	
	@Data
	private class Result {
		private String referenceKey ;
		private Boolean isValid ;
		private String rejectionReason ;
		private VehicleInfo vehicleInfo ;
		
		@Data
		private class VehicleInfo {
			private String licenseExpiryDateHijri ;
			private String brandArabic ;
			private String manufacturerArabic ;
			private String modelYear ;
			private String colorArabic ;
		}
	}
}
