package com.petroapp.wasl.dto.vehicle.delete;

import javax.validation.constraints.NotBlank;
import lombok.Data;

@Data
public class DeleteVehicleRequestDTO {

	@NotBlank(message = "operationCompanyReferenceKey Can't be blank")
	private String operationCompanyReferenceKey ;
	
	@NotBlank(message = "vehicleReferenceKey Can't be blank")
	private String vehicleReferenceKey ;
}
