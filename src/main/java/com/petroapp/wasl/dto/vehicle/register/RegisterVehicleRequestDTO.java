package com.petroapp.wasl.dto.vehicle.register;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import lombok.Data;

@Data
public class RegisterVehicleRequestDTO {

	@NotBlank(message = "sequenceNumber Can't be blank")
	private String sequenceNumber ;
	
	@NotNull(message = "vehiclePlate Can't be null")
	private VehiclePlate vehiclePlate ;
	
	@NotNull(message = "plateType Can't be null")
	private Integer plateType ;
	
	@NotBlank(message = "imeiNumber Can't be blank")
	private String imeiNumber ;
	
	@NotBlank(message = "operationCompanyReferenceKey Can't be blank")
	private String operationCompanyReferenceKey ;
	
	@Data
	private class VehiclePlate {
		@NotBlank(message = "number Can't be blank")
		private String number ;
		
		@NotBlank(message = "rightLetter Can't be blank")
		private String rightLetter ;
		
		@NotBlank(message = "middleLetter Can't be blank")
		private String middleLetter ;
		
		@NotBlank(message = "leftLetter Can't be blank")
		private String leftLetter ;
	}
}
