package com.petroapp.wasl.dto.vehicle.live.location;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import lombok.Data;

@Data
public class LiveLocartionRequestDTO {

	private VehicleLocations[] vehicleLocations ;
	
	@Data
	private class VehicleLocations {
		
		@NotBlank(message = "referenceKey Can't be blank")
		private String referenceKey ;
		
		@NotBlank(message = "driverReferenceKey Can't be blank")
		private String driverReferenceKey ;
		
		@NotNull(message = "latitude Can't be blank")
		private Double latitude ;
		
		@NotNull(message = "longitude Can't be blank")
		private Double longitude ;
		
		@NotNull(message = "velocity Can't be blank")
		private Double velocity ;
		
		@NotNull(message = "weight Can't be blank")
		private Double weight ;
		
		@NotBlank(message = "locationTime Can't be blank")
		private String locationTime ;
		
		@NotBlank(message = "vehicleStatus Can't be blank")
		private String vehicleStatus ;
		
		@NotBlank(message = "address Can't be blank")
		private String address ;
		private String roleCode = "T1" ;
		private Double temperature ;
		private Double humidity ;
	}
}
