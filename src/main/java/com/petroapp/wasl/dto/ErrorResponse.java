package com.petroapp.wasl.dto;

import lombok.Data;

@Data
public class ErrorResponse extends BaseResponse{

	private Error error ;
	
	@Data
	public static class Error {
		private String errorCode ;
		private String errorMsg ;
	}
}
