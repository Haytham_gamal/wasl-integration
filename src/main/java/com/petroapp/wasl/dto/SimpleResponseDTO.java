package com.petroapp.wasl.dto;

import lombok.Data;

@Data
public class SimpleResponseDTO extends BaseResponse {

	private String resultCode ;
	private boolean success ;
	private Result result ;
	
	@Data
	private static class Result {
		private Boolean isValid ;
		private String referenceKey ;
	}
	
}
