package com.petroapp.wasl.dto.driver.register;

import javax.validation.constraints.NotBlank;

import lombok.Data;

@Data
public class RegisterDriverRequestDTO {

	@NotBlank(message = "operatingCompanyReferenceKey Can't be blank")
	private String operatingCompanyReferenceKey ;
	
	@NotBlank(message = "operatingCompanyReferenceKey Can't be blank")
	private String identityNumber ;
	
	private String dateOfBirthHijri ;
	private String dateOfBirthGregorian ;
	
	@NotBlank(message = "operatingCompanyReferenceKey Can't be blank")
	private String mobileNumber ;
}
