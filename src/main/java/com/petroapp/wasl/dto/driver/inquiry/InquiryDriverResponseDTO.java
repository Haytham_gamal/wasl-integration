package com.petroapp.wasl.dto.driver.inquiry;

import lombok.Data;

@Data
public class InquiryDriverResponseDTO {

	private String referenceKey ;
	private String name ;
	private String registrationTime ;
	private OperationCompany[] operatingCompanies ;

	@Data
	private class OperationCompany {
		private String referenceKey ;
		private String name ;
		private Boolean isDriverValid ;
		private String driverRejectionReason ;
	}
}
