package com.petroapp.wasl.dto.driver.delete;

import javax.validation.constraints.NotBlank;
import lombok.Data;

@Data
public class DeleteDriverRequestDTO {

	@NotBlank(message = "operationCompanyReferenceKey Can't be blank")
	private String operationCompanyReferenceKey ;
	
	@NotBlank(message = "driverReferenceKey Can't be blank")
	private String driverReferenceKey ;
}
