package com.petroapp.wasl.dto.driver.update;

import javax.validation.constraints.NotBlank;

import lombok.Data;

@Data
public class UpdateDriverRequestDTO {

	@NotBlank(message =  "operatingCompanyReferenceKey Can't be blank")
	private String operatingCompanyReferenceKey ;
	
	@NotBlank(message =  "identityNumber Can't be blank")
	private String identityNumber;
	
	@NotBlank(message =  "mobileNumber Can't be blank")
	private String mobileNumber ;
	
	@NotBlank(message =  "email Can't be blank")
	private String email ;
}
