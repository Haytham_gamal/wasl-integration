package com.petroapp.wasl.dto.driver.register;

import lombok.Data;

@Data
public class RegisterDriverResponseDTO {

	private Boolean success ;
	private String resultCode ;
	private Result result ;
	
	@Data
	public static class Result {
		private String referenceKey ;
		private Boolean isValid ;
		private String rejectionReason ;
		private DriverInfo driverInfo ;
		
	}
	
	@Data
	public static class DriverInfo {
		private String nationalityArabic ;
		private String nameArabic ;
		private String licenseExpiryDateHijri ;
	}
}
