package com.petroapp.wasl.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.petroapp.wasl.dto.DeleteResponseDTO;
import com.petroapp.wasl.dto.SimpleResponseDTO;
import com.petroapp.wasl.dto.vehicle.delete.DeleteVehicleRequestDTO;
import com.petroapp.wasl.dto.vehicle.inquiry.InquiryVehicleResponseDTO;
import com.petroapp.wasl.dto.vehicle.live.location.LiveLocartionRequestDTO;
import com.petroapp.wasl.dto.vehicle.live.location.LiveLocationResponseDTO;
import com.petroapp.wasl.dto.vehicle.register.IMEIUpdateRequestDTO;
import com.petroapp.wasl.dto.vehicle.register.RegisterVehicleRequestDTO;
import com.petroapp.wasl.dto.vehicle.register.RegisterVehicleResponseDTO;
import com.petroapp.wasl.service.VehicleService;

@RestController
public class VechileController {

	@Autowired VehicleService vehicleService;
	
	@PostMapping("/vehicle")
	public RegisterVehicleResponseDTO registerVehicle (@Valid @RequestBody RegisterVehicleRequestDTO registerVehicleRequestDTO) throws JsonMappingException, JsonProcessingException {
		return vehicleService.registerVehicle(registerVehicleRequestDTO);
	}
	
	@GetMapping("/vehicle")
	public InquiryVehicleResponseDTO inquiryVehicles (@RequestParam(value = "sequenceNumber" , required = true) String sequenceNumber) {
		return vehicleService.vehicleInquiry(sequenceNumber);
	}
	
	@DeleteMapping("/vehicle")
	public DeleteResponseDTO deleteVehicle (@Valid @RequestBody DeleteVehicleRequestDTO deleteVehicleRequestDTO) {
		return vehicleService.deleteVehicle(deleteVehicleRequestDTO);
	}
	
	@PostMapping("/vehicle-live-locations")
	public LiveLocationResponseDTO sendLiveLocations (@Valid @RequestBody LiveLocartionRequestDTO liveLocartionRequestDTO) {
		return vehicleService.sendVehiclesLiveLocations(liveLocartionRequestDTO);
	}
	
	@PutMapping("/vehicle")
	public SimpleResponseDTO updateVehicleIMEI (@Valid @RequestBody IMEIUpdateRequestDTO imeiUpdateRequestDTO) {
		return vehicleService.updateVehicleIMEI(imeiUpdateRequestDTO);
	}
}
