package com.petroapp.wasl.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.petroapp.wasl.dto.DeleteResponseDTO;
import com.petroapp.wasl.dto.SimpleResponseDTO;
import com.petroapp.wasl.dto.driver.delete.DeleteDriverRequestDTO;
import com.petroapp.wasl.dto.driver.inquiry.InquiryDriverResponseDTO;
import com.petroapp.wasl.dto.driver.register.RegisterDriverRequestDTO;
import com.petroapp.wasl.dto.driver.register.RegisterDriverResponseDTO;
import com.petroapp.wasl.dto.driver.update.UpdateDriverRequestDTO;
import com.petroapp.wasl.service.DriverService;

@RestController
public class DriverController {

	@Autowired DriverService driverService;
	
	@PostMapping("/driver")
	public RegisterDriverResponseDTO registerDriver (@Valid @RequestBody RegisterDriverRequestDTO registerDriverRequestDTO) throws JsonMappingException, JsonProcessingException {
		return driverService.registerDriver(registerDriverRequestDTO);
	}
	
	@GetMapping("/driver")
	public InquiryDriverResponseDTO inquiryDrivers (@RequestParam(value = "sequenceNumber" , required = true) String sequenceNumber) {
		return driverService.driverInquiry(sequenceNumber);
	}
	
	@DeleteMapping("/driver")
	public DeleteResponseDTO deleteDriver (@Valid @RequestBody DeleteDriverRequestDTO deleteDriverRequestDTO) {
		return driverService.deleteDriver(deleteDriverRequestDTO);
	}
	
	@PutMapping("/driver")
	public SimpleResponseDTO updateDriver (@Valid @RequestBody UpdateDriverRequestDTO updateDriverRequestDTO) {
		return driverService.updateDriver(updateDriverRequestDTO);
	}
}
