package com.petroapp.wasl.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.petroapp.wasl.dto.BaseResponse;
import com.petroapp.wasl.dto.DeleteResponseDTO;
import com.petroapp.wasl.dto.operation.company.delete.DeleteRequestDTO;
import com.petroapp.wasl.dto.operation.company.inquiry.BaseEnquiryDTO;
import com.petroapp.wasl.dto.operation.company.inquiry.InquiryResponseDTO;
import com.petroapp.wasl.dto.operation.company.registeration.BaseRegisterDTO;
import com.petroapp.wasl.dto.operation.company.update.UpdateContactInfoRequestDTO;
import com.petroapp.wasl.dto.operation.company.update.UpdateContactInfoResponseDTO;
import com.petroapp.wasl.service.OperationCompanyService;

@RestController
public class OperationCompaniesController {

	@Autowired OperationCompanyService operationCompanyService;
	
	@PostMapping("/operation-company")
	public BaseResponse registerOperationCompany (@Valid @RequestBody BaseRegisterDTO baseRegisterDTO) throws JsonMappingException, JsonProcessingException {
		return operationCompanyService.registerOperatingCompany(baseRegisterDTO);
	}
	
	@PostMapping("/inquiry-operation-company")
	public InquiryResponseDTO inquiryOperationCompany (@Valid @RequestBody BaseEnquiryDTO baseEnquiryDTO) {
		return operationCompanyService.inquiryOnOperationCompany(baseEnquiryDTO);
	}
	
	@PostMapping("/delete-operation-company")
	public DeleteResponseDTO deleteOperationCompany (@Valid @RequestBody DeleteRequestDTO deleteRequestDTO) {
		return operationCompanyService.deleteOperationCompany(deleteRequestDTO);
	}
	
	@PatchMapping("/operation-company-contact-info")
	public UpdateContactInfoResponseDTO updateContactInfo (@Valid @RequestBody UpdateContactInfoRequestDTO updateContactInfoRequestDTO) {
		return operationCompanyService.updateContactInfo(updateContactInfoRequestDTO);
	}
}
