package com.petroapp.wasl.conf;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpHeaders;
import org.springframework.web.client.RestTemplate;

@Configuration
public class ConnectionConf {

	@Value("${wasl.api.key}")
	private String apiKey ;
	
	@Bean
	public RestTemplate initRestTemplate() {
		return new RestTemplate();
	}
	
	@Bean
	public HttpHeaders initHttpHeaders () {
		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.add("Content-Type", "application/json");
		httpHeaders.add("x-api-key", apiKey) ;
		
		return httpHeaders ;
	}
}
