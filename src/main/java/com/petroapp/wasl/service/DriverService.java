package com.petroapp.wasl.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.petroapp.wasl.dto.DeleteResponseDTO;
import com.petroapp.wasl.dto.SimpleResponseDTO;
import com.petroapp.wasl.dto.driver.delete.DeleteDriverRequestDTO;
import com.petroapp.wasl.dto.driver.inquiry.InquiryDriverResponseDTO;
import com.petroapp.wasl.dto.driver.register.RegisterDriverRequestDTO;
import com.petroapp.wasl.dto.driver.register.RegisterDriverResponseDTO;
import com.petroapp.wasl.dto.driver.update.UpdateDriverRequestDTO;

public interface DriverService {

	RegisterDriverResponseDTO registerDriver(RegisterDriverRequestDTO registerVehicleRequestDTO) throws JsonMappingException, JsonProcessingException;
	
	InquiryDriverResponseDTO driverInquiry (String sequenceNumber) ;

	DeleteResponseDTO deleteDriver (DeleteDriverRequestDTO deleteVehicleRequestDTO);
	
	SimpleResponseDTO updateDriver (UpdateDriverRequestDTO updateDriverRequestDTO);
}
