package com.petroapp.wasl.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.HttpStatusCodeException;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.petroapp.wasl.dto.DeleteResponseDTO;
import com.petroapp.wasl.dto.SimpleResponseDTO;
import com.petroapp.wasl.dto.driver.delete.DeleteDriverRequestDTO;
import com.petroapp.wasl.dto.driver.inquiry.InquiryDriverResponseDTO;
import com.petroapp.wasl.dto.driver.register.RegisterDriverRequestDTO;
import com.petroapp.wasl.dto.driver.register.RegisterDriverResponseDTO;
import com.petroapp.wasl.dto.driver.update.UpdateDriverRequestDTO;
import com.petroapp.wasl.service.DriverService;

@Service
@Transactional
public class DriverServiceImpl extends BaseService implements DriverService{

	Logger log = LoggerFactory.getLogger(DriverServiceImpl.class);
	
	@Override
	public RegisterDriverResponseDTO registerDriver(RegisterDriverRequestDTO registerDriverRequestDTO) throws JsonMappingException, JsonProcessingException {
		String url = apiUrl + "operating-companies/"+registerDriverRequestDTO.getOperatingCompanyReferenceKey()+"/drivers" ;
		log.info("calling Register Drivers at : " + url);
		
		HttpEntity<RegisterDriverRequestDTO> httpEntity = new HttpEntity<RegisterDriverRequestDTO>(registerDriverRequestDTO,httpHeaders);
		ObjectMapper mapper = new ObjectMapper();
		try {
			ResponseEntity<String> responseEntity = restTemplate.exchange(url,HttpMethod.POST, httpEntity, String.class);
			return mapper.readValue(responseEntity.getBody(), RegisterDriverResponseDTO.class);
		} catch (HttpStatusCodeException httpStatusCodeException) {
			return mapper.readValue(httpStatusCodeException.getResponseBodyAsString(), RegisterDriverResponseDTO.class);
		}
	}

	@Override
	public InquiryDriverResponseDTO driverInquiry(String sequenceNumber) {
		String url = apiUrl + "drivers?sequenceNumber=" + sequenceNumber ;
		
		log.info("calling driver inquiry at : " + url);
		HttpEntity<?> httpEntity = new HttpEntity<>(httpHeaders);
		
		ResponseEntity<InquiryDriverResponseDTO> responseEntity = restTemplate.exchange(url, HttpMethod.GET, httpEntity, InquiryDriverResponseDTO.class);
		return responseEntity.getBody();
	}

	@Override
	public DeleteResponseDTO deleteDriver(DeleteDriverRequestDTO deleteVehicleRequestDTO) {
		String url = apiUrl + "operating-companies/" + deleteVehicleRequestDTO.getOperationCompanyReferenceKey() + "/driver/" + deleteVehicleRequestDTO.getDriverReferenceKey();
		
		log.info("calling driver delete at : " + url);
		HttpEntity<?> httpEntity = new HttpEntity<>(httpHeaders);
		
		ResponseEntity<DeleteResponseDTO> responseEntity = restTemplate.exchange(url, HttpMethod.DELETE, httpEntity, DeleteResponseDTO.class);
		return responseEntity.getBody();
	}

	
	@Override
	public SimpleResponseDTO updateDriver(UpdateDriverRequestDTO updateDriverRequestDTO) {
		String url = apiUrl + "operating-companies/"+updateDriverRequestDTO.getOperatingCompanyReferenceKey()+"/drivers" ;
		log.info("calling update Drivers at : " + url);
		
		HttpEntity<UpdateDriverRequestDTO> httpEntity = new HttpEntity<UpdateDriverRequestDTO>(updateDriverRequestDTO,httpHeaders);
		ResponseEntity<SimpleResponseDTO> responseEntity = restTemplate.exchange(url, HttpMethod.PUT, httpEntity, SimpleResponseDTO.class);
		return responseEntity.getBody();
	}

}
