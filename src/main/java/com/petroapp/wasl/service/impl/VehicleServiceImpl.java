package com.petroapp.wasl.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.HttpStatusCodeException;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.petroapp.wasl.dto.DeleteResponseDTO;
import com.petroapp.wasl.dto.SimpleResponseDTO;
import com.petroapp.wasl.dto.vehicle.delete.DeleteVehicleRequestDTO;
import com.petroapp.wasl.dto.vehicle.inquiry.InquiryVehicleResponseDTO;
import com.petroapp.wasl.dto.vehicle.live.location.LiveLocartionRequestDTO;
import com.petroapp.wasl.dto.vehicle.live.location.LiveLocationResponseDTO;
import com.petroapp.wasl.dto.vehicle.register.IMEIUpdateRequestDTO;
import com.petroapp.wasl.dto.vehicle.register.RegisterVehicleRequestDTO;
import com.petroapp.wasl.dto.vehicle.register.RegisterVehicleResponseDTO;
import com.petroapp.wasl.service.VehicleService;

@Service
@Transactional
public class VehicleServiceImpl extends BaseService implements VehicleService{

	Logger log = LoggerFactory.getLogger(VehicleServiceImpl.class);
	
	@Override
	public RegisterVehicleResponseDTO registerVehicle(RegisterVehicleRequestDTO registerVehicleRequestDTO) throws JsonMappingException, JsonProcessingException {
		String url = apiUrl + "operating-companies/"+registerVehicleRequestDTO.getOperationCompanyReferenceKey()+"/vehicles" ;
		log.info("calling Register Vehicle at : " + url);
		
		HttpEntity<RegisterVehicleRequestDTO> httpEntity = new HttpEntity<RegisterVehicleRequestDTO>(registerVehicleRequestDTO,httpHeaders);
		ObjectMapper mapper = new ObjectMapper();
		try {
			ResponseEntity<String> responseEntity = restTemplate.exchange(url,HttpMethod.POST, httpEntity, String.class);
			return mapper.readValue(responseEntity.getBody(), RegisterVehicleResponseDTO.class);
		} catch (HttpStatusCodeException httpStatusCodeException) {
			return mapper.readValue(httpStatusCodeException.getResponseBodyAsString(), RegisterVehicleResponseDTO.class);
		}
	}
	
	@Override
	public InquiryVehicleResponseDTO vehicleInquiry(String sequenceNumber) {
		String url = apiUrl + "vehicles?sequenceNumber=" + sequenceNumber ;
		
		log.info("calling Vehicle inquiry at : " + url);
		HttpEntity<?> httpEntity = new HttpEntity<RegisterVehicleRequestDTO>(httpHeaders);
		
		ResponseEntity<InquiryVehicleResponseDTO> responseEntity = restTemplate.exchange(url, HttpMethod.GET, httpEntity, InquiryVehicleResponseDTO.class);
		return responseEntity.getBody();
	}
	
	@Override
	public DeleteResponseDTO deleteVehicle(DeleteVehicleRequestDTO deleteVehicleRequestDTO) {
		String url = apiUrl + "operating-companies/" + deleteVehicleRequestDTO.getOperationCompanyReferenceKey() + "/vehicle/" + deleteVehicleRequestDTO.getVehicleReferenceKey();
		
		log.info("calling Vehicle delete at : " + url);
		HttpEntity<?> httpEntity = new HttpEntity<RegisterVehicleRequestDTO>(httpHeaders);
		
		ResponseEntity<DeleteResponseDTO> responseEntity = restTemplate.exchange(url, HttpMethod.DELETE, httpEntity, DeleteResponseDTO.class);
		return responseEntity.getBody();
	}

	@Override
	public LiveLocationResponseDTO sendVehiclesLiveLocations(LiveLocartionRequestDTO liveLocartionRequestDTO) {
		String url = apiUrl + "locations" ;
		
		log.info("calling send vehicles live locations at : " + url);
		HttpEntity<LiveLocartionRequestDTO> httpEntity = new HttpEntity<LiveLocartionRequestDTO>(liveLocartionRequestDTO,httpHeaders);
		
		ResponseEntity<LiveLocationResponseDTO> responseEntity = restTemplate.exchange(url, HttpMethod.POST, httpEntity, LiveLocationResponseDTO.class);
		return responseEntity.getBody();
	}

	@Override
	public SimpleResponseDTO updateVehicleIMEI(IMEIUpdateRequestDTO imeiUpdateRequestDTO) {
		String url = apiUrl + "vehicles/imei" ;
		
		log.info("calling update vehicles imei at : " + url);
		HttpEntity<IMEIUpdateRequestDTO> httpEntity = new HttpEntity<IMEIUpdateRequestDTO>(imeiUpdateRequestDTO,httpHeaders);
		
		ResponseEntity<SimpleResponseDTO> responseEntity = restTemplate.exchange(url, HttpMethod.PUT, httpEntity, SimpleResponseDTO.class);
		return responseEntity.getBody();
	}

}
