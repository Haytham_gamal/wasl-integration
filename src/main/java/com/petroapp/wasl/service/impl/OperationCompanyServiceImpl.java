package com.petroapp.wasl.service.impl;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpStatusCodeException;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.petroapp.wasl.dto.BaseResponse;
import com.petroapp.wasl.dto.DeleteResponseDTO;
import com.petroapp.wasl.dto.ErrorResponse;
import com.petroapp.wasl.dto.SimpleResponseDTO;
import com.petroapp.wasl.dto.operation.company.delete.DeleteRequestDTO;
import com.petroapp.wasl.dto.operation.company.inquiry.BaseEnquiryDTO;
import com.petroapp.wasl.dto.operation.company.inquiry.CompanyInquiryRequestDTO;
import com.petroapp.wasl.dto.operation.company.inquiry.InquiryResponseDTO;
import com.petroapp.wasl.dto.operation.company.registeration.BaseRegisterDTO;
import com.petroapp.wasl.dto.operation.company.update.UpdateContactInfoRequestDTO;
import com.petroapp.wasl.dto.operation.company.update.UpdateContactInfoResponseDTO;
import com.petroapp.wasl.service.OperationCompanyService;

@Service
public class OperationCompanyServiceImpl extends BaseService implements OperationCompanyService{

	Logger log = LoggerFactory.getLogger(OperationCompanyServiceImpl.class);
	
	@Override
	public BaseResponse registerOperatingCompany(BaseRegisterDTO baseRegisterDTO)
			throws JsonMappingException, JsonProcessingException {
		log.info("calling Registeration at : " + apiUrl + "operating-companies");
		log.info("data is : " + baseRegisterDTO.toString() );
		HttpEntity<BaseRegisterDTO> httpEntity = new HttpEntity<BaseRegisterDTO>(baseRegisterDTO, httpHeaders);
		ObjectMapper mapper = new ObjectMapper();
		try {
			ResponseEntity<String> responseEntity = restTemplate.exchange(apiUrl + "operating-companies",
					HttpMethod.POST, httpEntity, String.class);
			return mapper.readValue(responseEntity.getBody(), SimpleResponseDTO.class);
		} catch (HttpStatusCodeException httpStatusCodeException) {
			if(httpStatusCodeException.getStatusCode() == HttpStatus.BAD_REQUEST) {
				return mapper.readValue(httpStatusCodeException.getResponseBodyAsString(), SimpleResponseDTO.class);
			}else {
				ErrorResponse.Error errors = mapper.readValue(httpStatusCodeException.getResponseBodyAsString(),new TypeReference<ErrorResponse.Error>() {});
				ErrorResponse errorResponse = new ErrorResponse();
				errorResponse.setError(errors);
				return errorResponse;
			}
			
		}
	}

	@Override
	public InquiryResponseDTO inquiryOnOperationCompany(BaseEnquiryDTO baseEnquiryDTO) {
		String url = apiUrl + "operating-companies?identityNumber="+ baseEnquiryDTO.getIdentityNumber();
		if(baseEnquiryDTO instanceof CompanyInquiryRequestDTO) {
			url = url + "&commercialRecordNumber=" + ((CompanyInquiryRequestDTO)baseEnquiryDTO).getCommercialRecordNumber();
		}
		log.info("calling Inquiry at : " + url);

		HttpEntity<?> httpEntity = new HttpEntity<>(httpHeaders);
		ResponseEntity<InquiryResponseDTO> responseEntity = restTemplate.exchange(url , HttpMethod.GET, httpEntity, InquiryResponseDTO.class);
		return responseEntity.getBody();
	}

	@Override
	public DeleteResponseDTO deleteOperationCompany(DeleteRequestDTO deleteRequestDTO) {
		String url = apiUrl + "operating-companies/"+ deleteRequestDTO.getReferenceKey();
		
		log.info("calling Delete at : " + url);
		
		HttpEntity<?> httpEntity = new HttpEntity<>(httpHeaders);
		ResponseEntity<DeleteResponseDTO> responseEntity = restTemplate.exchange(url , HttpMethod.DELETE, httpEntity, DeleteResponseDTO.class);
		return responseEntity.getBody();
	}

	@Override
	public UpdateContactInfoResponseDTO updateContactInfo(UpdateContactInfoRequestDTO updateContactInfoRequestDTO) {
		String url = apiUrl + "operating-companies/contact-info" ;
		
		log.info("calling update operating-companies contact info at : " + url);
		
		HttpEntity<UpdateContactInfoRequestDTO> httpEntity = new HttpEntity<UpdateContactInfoRequestDTO>(updateContactInfoRequestDTO,httpHeaders);
		ResponseEntity<UpdateContactInfoResponseDTO> responseEntity = restTemplate.exchange(url , HttpMethod.PATCH, httpEntity, UpdateContactInfoResponseDTO.class);
		return responseEntity.getBody();
	}

}
