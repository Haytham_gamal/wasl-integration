package com.petroapp.wasl.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.web.client.RestTemplate;

public abstract class BaseService {

	@Value("${wasl.api.url}")
	String apiUrl ;
	
	@Autowired RestTemplate restTemplate ;
	@Autowired HttpHeaders httpHeaders;
}
