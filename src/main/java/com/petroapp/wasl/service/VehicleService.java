package com.petroapp.wasl.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.petroapp.wasl.dto.DeleteResponseDTO;
import com.petroapp.wasl.dto.SimpleResponseDTO;
import com.petroapp.wasl.dto.vehicle.delete.DeleteVehicleRequestDTO;
import com.petroapp.wasl.dto.vehicle.inquiry.InquiryVehicleResponseDTO;
import com.petroapp.wasl.dto.vehicle.live.location.LiveLocartionRequestDTO;
import com.petroapp.wasl.dto.vehicle.live.location.LiveLocationResponseDTO;
import com.petroapp.wasl.dto.vehicle.register.IMEIUpdateRequestDTO;
import com.petroapp.wasl.dto.vehicle.register.RegisterVehicleRequestDTO;
import com.petroapp.wasl.dto.vehicle.register.RegisterVehicleResponseDTO;

public interface VehicleService {

	RegisterVehicleResponseDTO registerVehicle(RegisterVehicleRequestDTO registerVehicleRequestDTO) throws JsonMappingException, JsonProcessingException;
	
	InquiryVehicleResponseDTO vehicleInquiry (String sequenceNumber) ;
	
	DeleteResponseDTO deleteVehicle (DeleteVehicleRequestDTO deleteVehicleRequestDTO);
	
	LiveLocationResponseDTO sendVehiclesLiveLocations (LiveLocartionRequestDTO liveLocartionRequestDTO);
	
	SimpleResponseDTO updateVehicleIMEI (IMEIUpdateRequestDTO imeiUpdateRequestDTO);
}
