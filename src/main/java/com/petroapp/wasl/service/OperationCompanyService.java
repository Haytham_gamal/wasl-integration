package com.petroapp.wasl.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.petroapp.wasl.dto.BaseResponse;
import com.petroapp.wasl.dto.DeleteResponseDTO;
import com.petroapp.wasl.dto.SimpleResponseDTO;
import com.petroapp.wasl.dto.operation.company.delete.DeleteRequestDTO;
import com.petroapp.wasl.dto.operation.company.inquiry.BaseEnquiryDTO;
import com.petroapp.wasl.dto.operation.company.inquiry.InquiryResponseDTO;
import com.petroapp.wasl.dto.operation.company.registeration.BaseRegisterDTO;
import com.petroapp.wasl.dto.operation.company.update.UpdateContactInfoRequestDTO;
import com.petroapp.wasl.dto.operation.company.update.UpdateContactInfoResponseDTO;

public interface OperationCompanyService {

	BaseResponse registerOperatingCompany(BaseRegisterDTO baseRegisterDTO) throws JsonMappingException, JsonProcessingException ;
	
	InquiryResponseDTO inquiryOnOperationCompany(BaseEnquiryDTO baseEnquiryDTO);
	
	DeleteResponseDTO deleteOperationCompany(DeleteRequestDTO deleteRequestDTO);
	
	UpdateContactInfoResponseDTO updateContactInfo(UpdateContactInfoRequestDTO updateContactInfoRequestDTO);
}
